<?php

namespace App;


class Registration
{
    private $student;
    private $course;
    private $class = '';
    private $note = 0;

    /**
     * @param \App\Student $student
     * @param \App\Course  $course
     * @param string       $class
     * @param int          $note
     */
    public function __construct(Student $student, Course $course, $class = '', $note = 0)
    {
        $this->student = $student;
        $this->course = $course;
        $this->class = $class;
        $this->note = $note;
    }

    /**
     * @return \App\Student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @return \App\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return int
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param int $a1
     * @param int $a2
     * @param int $a3
     */
    public function setNote($a1 = 0, $a2 = 0, $a3 = 0)
    {
        $this->note = array_sum([$a1, $a2, $a3]);
    }
}