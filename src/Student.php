<?php

namespace App;

class Student
{
    private $name = null;
    private $age = null;
    private $motherName = null;
    private $fatherName = null;
    private $cpf = null;

    public function __construct($name, $age, $motherName, $fatherName, $cpf)
    {
        $this->name = $name;
        $this->age = $age;
        $this->motherName = $motherName;
        $this->fatherName = $fatherName;
        $this->cpf = $cpf;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getMotherName()
    {
        return $this->motherName;
    }

    public function getFatherName()
    {
        return $this->fatherName;
    }

    public function getCpf()
    {
        return $this->cpf;
    }
}