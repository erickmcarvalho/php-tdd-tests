<?php

require_once '../bootstrap.php';

class StudentTest extends PHPUnit_Framework_TestCase
{
    private $student;

    public function __construct()
    {
        parent::__construct();

        $this->student = new \App\Student('Bruno Lorenzo Oliveira', 22, 'Mariana Vit�ria Let�cia Pereira', 'LUSI VITOR MOTTA CARVALHO', '455.145.336-62');
    }

    public function testType()
    {

        //$student = new \App\Student(1, '1', 1, 1, 1);

        $this->assertInternalType('string', $this->student->getName());
        $this->assertInternalType('int', $this->student->getAge());
        $this->assertInternalType('string', $this->student->getMotherName());
        $this->assertInternalType('string', $this->student->getFatherName());
        $this->assertInternalType('string', $this->student->getCpf());
    }

    public function testCpf()
    {
        $this->assertEquals('ERICK ANTHONY DE OLIVEIRA CARVALHO')
    }
}
